;
(function ($) {
    $('.js-edit-review').each(function (index, elem) {
        var $editBtn = $(elem).find('.js-edit-review__change'),
            $closeBtn = $(elem).find('.js-edit-review__close'),
            $editBlock = $(elem).find('.js-edit-review__edit');
        $editBtn.click(function (event) {
            event.preventDefault();
            $editBlock.toggle(true);
        });
        $closeBtn.click(function (event) {
            event.preventDefault();
            $editBlock.toggle(false);
        });
    });
    $('.js-medical-history').MGToggle();
    $('.js-tabs').MGTabs({
        contentWrapper: '.sidemenu-tab-content'
    });
})(jQuery);