;
(function($) {

    /**
     * Позиционирование треугольника дропдауна
     * - блок треугольника должен иметь класс 'mg-dropdown-arrow'
     * - иконка, относительно которой необходимо позиционировать, должна
     *   иметь аттрибут dropdown-arrow="true"
     */
    function fixDropdownArrow() {
        $('.mg-dropdown-arrow').each(function () {
            var $this = $(this),
                $dropdownArrow = $this.closest('.dropdown').find('[dropdown-arrow="true"]'),
                arrowOffsetLeft = $dropdownArrow.offset().left,
                parentOffsetLeft = $dropdownArrow.parent().offset().left;
            $this.css('left', (arrowOffsetLeft - parentOffsetLeft) + 2 + 'px');
        });
    }

    $(document).ready(function(){
        /**
         * Запинить хедер
         */
        $("#sticker").sticky({
           topSpacing:0
        });

        fixDropdownArrow();



        $('a[data-toggle="tab"]').on('shown.bs.tab', function () {
            fixDropdownArrow();
        })
    });
})(jQuery);