(function ($) {
    var defaults = {
        contentWrapper: '.tab-content',
        contentElem: '.tab-pane',
        nested: false,
        onChange: null
    };
    $.fn.MGTabs = function (options) {
        var settings = $.extend(defaults, options);
        return this.each(function(ind, elem) {
            $contentWrapper = $(settings.contentWrapper);
            $link = $(this).find('.mg-tab-link');
            $link.on('click', function (e) {
                e.preventDefault();
                var item = $(this),
                    itemValue = item.data('tab');
                if (settings.nested) {
                    $(elem)
                        .find('.mg-tab-link')
                        .removeClass('active');
                    item.addClass('active');
                } else {
                    item.addClass('active')
                        .closest('.mg-tab-item')
                        .siblings()
                        .find('.mg-tab-link')
                        .removeClass('active');
                }
                $contentWrapper.find(settings.contentElem + '#' + itemValue)
                    .addClass('active')
                    .siblings()
                    .removeClass('active');
                if (settings.onChange) {
                    settings.onChange(itemValue);
                }
            });
        });
    }
})(jQuery);