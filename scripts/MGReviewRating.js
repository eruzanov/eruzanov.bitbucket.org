(function ($) {
    $.fn.MGReviewRating = function () {
        return this.each(function() {
            var $this = $(this),
                $button = $this.find('button'),
                $input = $this.find('input[type="hidden"]'),
                rate = 3;
            $input.val(rate);
            $button.on('click', function() {
                rate = +$(this).attr('rate');
                $input.val(rate);
                $button.each(function (ind, button) {
                    if (rate >= +$(button).attr('rate')) {
                        $(button).addClass('create-review-rate__value_active');
                    } else {
                        $(button).removeClass('create-review-rate__value_active');
                    }
                })
            });
        });
    }
})(jQuery);