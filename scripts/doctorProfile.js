;
(function ($) {
    $(document).ready(function() {
        $('.js-rating').MGRating();
    });
    $('.js-answer-review').each(function (index, elem) {
        var $answerBtn = $(elem).find('.js-answer-review__reply'),
            $closeBtn = $(elem).find('.js-answer-review__close'),
            $answerBlock = $(elem).find('.js-answer-review__answer');
        $answerBtn.click(function (event) {
            event.preventDefault();
            $answerBlock.toggle(true);
        });
        $closeBtn.click(function (event) {
            event.preventDefault();
            $answerBlock.toggle(false);
        });
    });
    $('.js-tabs').MGTabs({
        contentWrapper: '.sidemenu-tab-content',
        nested: true
    });
})(jQuery);