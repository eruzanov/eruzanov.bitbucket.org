;
function ScheduleInterval() {
    this.start;
    this.last;
}
ScheduleInterval.prototype.setStart = function (hour) {
    this.start = +hour;
    this.last = +hour;
};
ScheduleInterval.prototype.setEnd = function(hour) {
    if (+hour >= this.start) {
        this.last = +hour;
    }
};

function ScheduleDay(element, activeHours) {
    this.element = $(element);
    this.activeHours = activeHours || [];
    this.innerElements = [];
    this.interval = null;
    this.HOURS = 24;
    this.isIntervalMaking = false;

    this.init();
}
ScheduleDay.prototype.addInterval = function (interval) {
    var i = interval.start;
    while (i <= interval.last) {
        if (this.activeHours.indexOf(i) == -1) {
            this.activeHours.push(i);
        }
        i++;
    }
    this.activeHours.sort(function (a, b) {
        return a - b;
    });
};
ScheduleDay.prototype.removeInterval = function (value) {
    var index = this.activeHours.indexOf(value);
    if (index == -1) return;
    this.activeHours.splice(index, 1);
    for (var i = value - 1; i >= 0; i--) {
        var index = this.activeHours.indexOf(i);
        if (index != -1) {
            this.activeHours.splice(index, 1);
        } else {
            break;
        }
    }
    for (var i = value + 1; i <= 23; i++) {
        var index = this.activeHours.indexOf(i);
        if (index != -1) {
            this.activeHours.splice(index, 1);
        } else {
            break;
        }
    }
    this.updateActiveHours();

};
ScheduleDay.prototype.updateActiveHours = function () {
    var activeList = [],
        ind = null;
    for (var j = 0; j < this.activeHours.length; j++) {
        activeList.push(this.activeHours[j]);
    }
    if (this.interval) {
        ind = this.interval.start;
        while (ind <= this.interval.last) {
            activeList.push(ind);
            ind++;
        }
    }
    for (var i = 0; i < this.innerElements.length; i++) {
        if (activeList.indexOf(+this.innerElements[i].attr('hours')) != -1) {
            this.innerElements[i].addClass('active');
        } else {
            this.innerElements[i].removeClass('active');
        }
    }
};

ScheduleDay.prototype.init = function () {
    for (var i = 0; i < this.HOURS; i++) {
        var timeBlock = $('<span />')
            .addClass('doctor-daily-schedule__elem')
            .attr('hours', i)
            .html((i > 9 ? i : '0' + i) + ':00');
        this.element.append(timeBlock);
        this.innerElements.push(timeBlock);
        initEvents.apply(this, timeBlock);
    }
    this.updateActiveHours();
};

function initEvents(timeBlock) {
    var self = this;
    $(timeBlock).on('mousedown', function () {
        var value = $(this).attr('hours');
        if (!self.isIntervalMaking) {
            self.interval = new ScheduleInterval();
            self.isIntervalMaking = true;
            self.interval.setStart(+value);
        }

    });
    $(timeBlock).on('mouseover', function () {
        var value = $(this).attr('hours');
        if (self.isIntervalMaking) {
            self.interval.setEnd(+value);
            self.updateActiveHours();
        }
    });
    $(document).on('mouseup', function () {
        if (self.isIntervalMaking) {
            self.isIntervalMaking = false;
            self.addInterval(self.interval);
            self.interval = null;
            self.updateActiveHours();
        }
    });
    $(timeBlock).on('dblclick', function () {
        var value = $(this).attr('hours');
        self.interval = null;
        self.removeInterval(+value);
    })
}



new ScheduleDay($('.js-mg-schedule_monday').get(0), [9, 10, 11, 12, 13, 15, 16, 17, 18, 19, 20]);
new ScheduleDay($('.js-mg-schedule_tuesday').get(0), [9, 10, 11, 12, 13, 15, 16, 17, 18, 19, 20]);
new ScheduleDay($('.js-mg-schedule_wednesday').get(0), [9, 10, 11, 12, 13, 15, 16, 17, 18, 19, 20]);
new ScheduleDay($('.js-mg-schedule_thursday').get(0), [9, 10, 11, 12, 13, 15, 16, 17, 18, 19, 20]);
new ScheduleDay($('.js-mg-schedule_friday').get(0), [9, 10, 11, 12, 13, 15, 16, 17, 18, 19, 20]);
new ScheduleDay($('.js-mg-schedule_saturday').get(0), [8, 9, 10, 11, 12]);
new ScheduleDay($('.js-mg-schedule_sunday').get(0), []);