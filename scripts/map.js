var initMap = (function ($) {
    var styles = [
        {
            "featureType": "landscape",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#eaeaea"
                }
            ]
        },
        {
            "featureType": "landscape.man_made",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "color": "#cbdce8"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "all",
            "stylers": [
                {
                    "color": "#eaeaea"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#bfd3e4"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#90aec2"
                }
            ]
        }
    ];
    MarkerOverlay.prototype = new google.maps.OverlayView();
    function MarkerOverlay(map, marker) {
        this.map_ = map;
        this.coords_ = marker.coords;
        this.title_ = marker.title;
        this.div_ = null;
        this.setMap(map);
    }
    MarkerOverlay.prototype.onAdd = function () {
        var div = $('<div />')
            .addClass('gmap-marker')
            .html(this.title_);

        this.div_ = div;

        // Add the element to the "overlayLayer" pane.
        var panes = this.getPanes();
        panes.overlayLayer.appendChild(div.get(0));
    };
    MarkerOverlay.prototype.draw = function () {
        var overlayProjection = this.getProjection();
        var pos = overlayProjection.fromLatLngToDivPixel(this.coords_);
        var div = this.div_;
        div.css({
            'left': pos.x + 'px',
            'top': (pos.y - 47) + 'px'
        });
    };


    return function initMap(element, markers) {
        if (markers.length === 0) {
            markers.push({
                coords: new google.maps.LatLng(55.8275954, 49.1302422)
            });
        }
        var styledMap = new google.maps.StyledMapType(styles,
            {name: "Styled Map"});
        var mapOptions = {
            center: markers[0].coords,
            zoom: 13,
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
            },
            disableDefaultUI: true
        };
        var map = new google.maps.Map($(element).get(0), mapOptions);
        map.mapTypes.set('map_style', styledMap);
        map.setMapTypeId('map_style');

        markers.forEach(function (marker) {
            new MarkerOverlay(map, marker);
        });
    };
})(jQuery);