(function ($) {
    var defaults = {
        bgColor: '#52b296',
        height: 4,
        total: 100,
        current: 0
    };
    $.fn.MGRating = function () {
        return this.each(function() {
            var settings = {
                bgColor: $(this).attr('color') || defaults.bgColor,
                height: $(this).attr('height') || defaults.height,
                total: $(this).attr('total') || defaults.total,
                current: $(this).attr('current') || defaults.current
            };
            var width = settings.current * 100 / settings.total;
            var inner = $('<div />')
                .addClass(width < 98 ? 'linear-rating-line__current linear-rating-line__current_bordered' : 'linear-rating-line__current')
                .css({
                    'background-color': settings.bgColor,
                    'width': width + '%'
                });
            var outer = $('<div />')
                .addClass('linear-rating-line')
                .css({
                    'height': settings.height + 'px'
                })
                .append(inner)
                .appendTo($(this));

        });
    }
})(jQuery);