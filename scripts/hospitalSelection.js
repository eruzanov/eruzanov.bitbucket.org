;
(function($){
    var isMapInitialized = false;

    $(document).ready(function () {
        $('#info').perfectScrollbar();
        $('#reviews').perfectScrollbar();
        $('.js-prof-options').perfectScrollbar();
        $('.js-rating').MGRating();
        $('.mg-popover-link').popover({
            html : true,
            content: function() {
                return $(this).parent().find('.js-popover-content').html();
            },
            placement: $(window).width() > 767 ? 'bottom' : 'left',
            template: '<div class="popover rate-info-popover" role="tooltip"><div class="popover-arrow"></div><div class="popover-content"></div></div>'
        });

        $(".js-doctor-sort-menu a").click(function(e){
            e.preventDefault();
            var selText = $(this).text();
            $(this).parents('.dropdown').find('.js-search-dropdown-text').html(selText);
        });

        $('.js-prof').each(function() {
            var $this = $(this),
                header = $this.find('.js-prof-header'),
                selectAllBtn = $this.find('.js-prof-select-all'),
                discardBtn = $this.find('.js-prof-discard'),
                optionsList = $this.find('.js-prof-options').find('[type="checkbox"]');
            header.on('click', function () {
                if ($this.hasClass('active')) {
                    $this.removeClass('active');
                } else {
                    $this.addClass('active');
                }
            });
            selectAllBtn.on('click', function () {
                optionsList.prop('checked', true);

            });
            discardBtn.on('click', function () {
                optionsList.prop('checked', false);
            });

            $(document).on('click', function (e) {
                if (! $this.is(e.target) && $this.has(e.target).length === 0) {
                    $this.removeClass('active');
                }
            })
        });

        function initMapInTab() {
            initMap('#map', [
                {
                    title: 'Адоратского 4',
                    coords: new google.maps.LatLng(55.8219507, 49.1488347)
                },
                {
                    title: 'Чистопольская 15',
                    coords: new google.maps.LatLng(55.819818,49.1107703)
                }
            ]);
        }

        $('.js-tabs').MGTabs({
            contentWrapper: '.selected-tab-content',
            onChange: function(type) {
                if (type === 'location' && !isMapInitialized) {
                    initMapInTab();
                    isMapInitialized = true;
                }
            }
        });
    });
})(jQuery);