;
(function ($) {
    function checkSelection() {
        $('.js-section').each(function () {
            var
                topEdge = $(this).offset().top - 200,
                bottomEdge = $(this).height() + topEdge,
                windowScroll = $(window).scrollTop();
            if (windowScroll > topEdge && windowScroll < bottomEdge) {
                var
                    current = $(this).data('section'),
                    link = $('.page-nav__item-link').filter('[href="#' + current + '"]');
                link.addClass('active').parent().siblings().find('.page-nav__item-link').removeClass('active');
                link.addClass('active');
            }
        })
    }

    function moveSelection(link) {
        $('html, body').animate({scrollTop: $('.js-section').filter('[data-section="' + link + '"]').offset().top - 50}, 500);
    }

    $(window).scroll(function () {
        checkSelection();
    });

    $(document).ready(function() {
        // Рейтинг
        $('.js-rating').MGRating();

        // Табы отзывов
        $('.js-tabs').MGTabs();

        // Скролл к блоку
        $('.page-nav__item-link').on('click', function(e) {
            e.preventDefault();
            var link = $(this).attr('href').split('#')[1];
            moveSelection(link);
        });

        // Модальное окно с видео
        var videourl = $('#mgvideo').attr('src');
        $('#videoModal').on('hide.bs.modal', function(){
            $("#mgvideo").attr('src', '');
        });
        $('#videoModal').on('show.bs.modal', function(){
            $("#mgvideo").attr('src', videourl);
        });
        $('#mgvideo').reframe();
    });
})(jQuery);
