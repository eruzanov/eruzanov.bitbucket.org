;
(function ($) {
    $(document).ready(function () {
        $('.js-doctor-prof-info').MGToggle({
            SHOW_TEXT: 'Показать полную информацию',
            HIDE_TEXT: 'Скрыть полную информацию'
        });

        initMap('#map', [
            {
                title: 'Адоратского 4',
                coords: new google.maps.LatLng(55.8219507, 49.1488347)
            },
            {
                title: 'Чистопольская 15',
                coords: new google.maps.LatLng(55.819818,49.1107703)
            }
        ]);
        $('.js-rating').MGRating();
        $('.js-review-rating').MGReviewRating();
        $('.mg-popover-link').popover({
            html : true,
            content: function() {
                return $('#popover').html();
            },
            placement: 'bottom',
            template: '<div class="popover rate-info-popover" role="tooltip"><div class="popover-arrow"></div><div class="popover-content"></div></div>'
        });
    });
})(jQuery);