(function ($) {
    var defaults = {
        HIDE_TEXT: 'Свернуть',
        SHOW_TEXT: 'Раскрыть полностью',
        CLASSNAME: 'active'
    };
    $.fn.MGToggle = function (options) {
        var settings = $.extend(defaults, options);
        return this.each(function() {
            var $content = $(this).find('.js-toggle-content'),
                $toggleBtn = $(this).find('.js-toggle-btn'),
                $toggleBtnWrap = $(this).find('.js-toggle-btn-wrap'),
                hasInnerIcon = $toggleBtn.children('.icon').length > 0,
                $innerSpan = hasInnerIcon ? $toggleBtn.find('span') : null;

            $toggleBtn.on('click', function (e) {
                e.preventDefault();
                if ($content.hasClass(settings.CLASSNAME)) {
                    $content
                        .add($toggleBtnWrap)
                        .removeClass(settings.CLASSNAME);
                    hasInnerIcon ? $innerSpan.html(settings.SHOW_TEXT) : $toggleBtn.html(settings.SHOW_TEXT);
                } else {
                    $content
                        .add($toggleBtnWrap)
                        .addClass(settings.CLASSNAME);
                    hasInnerIcon ? $innerSpan.html(settings.HIDE_TEXT) : $toggleBtn.html(settings.HIDE_TEXT);
                }
            });

        });
    }
})(jQuery);
